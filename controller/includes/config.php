<?php 
abstract class DBConfig 
{
	private $host;
	private $user;
	private $pass;
	private $db_name;

	public function __construct($host, $user, $pass, $db_name )
	{
		$this->host    = $host;
		$this->user    = $user;
		$this->pass    = $pass;
		$this->db_name = $db_name; 
	}
}

?>