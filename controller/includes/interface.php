<?php 
interface DBInterface
{
	public function dbConnect();
	public function insert($username, $emailid, $password,$hash);
	public function read();
	public function update($where);
	public function delete($where);
}
?>